var express = require('express');
var router = express.Router();
var url = require('url');


/* GET users listing. */
router.get('/', function(req, res, next) {

  let url_parts = url.parse(req.url, true);
  let query = url_parts.query;

  res.render('user', { title: 'Users Page',username:query.name , number:query.number , email:query.email });
});

module.exports = router;
