var express = require('express');
var router = express.Router();
const multer = require('multer');
const upload = multer({
  dest: '../uploads/' // this saves your file into a directory called "uploads"
}); 

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

//File upload
router.post('/file', upload.single('file-to-upload'), (req, res) => {
  console.log('File Upload Successful-------->',)
  res.redirect('/');
});

//form Submission
router.post('/form', upload.single('file-to-upload'), (req, res) => {
  console.log('User Data-------->',req.body)
  res.redirect(`/users?name=${req.body.name}&&number=${req.body.number}&&email=${req.body.email}`);
});

module.exports = router;
